<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-AU-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Michał Borzęcki</title>
    <link href="https://fonts.googleapis.com/css?family=Russo+One" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
     <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
   
   
   
  <div class="container">
      <div class="row">
        <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".my-navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Michał Borzęcki</a>
            </div>
            <div class="collapse navbar-collapse my-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="hidden">
                        <a class="page-scroll" href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">O mnie</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#offer">Oferta</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Portfolio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Kontakt</a>
                    </li>
                </ul>
        </div>
    </nav>
      </div>
      <div class="row row_main ">
          <div class="col-lg-6 col-lg-push-3 text-center">
              <img src="src/pic/photo-face.png" alt="My photo">
          </div>
        </div>
      <div class="row row_o_mnie">
          <div class="col-lg-6 col-lg-push-3 text-center">
              <h1 id="about">O mnie</h1>
              <h4>
                  <p>
                  Jestem Studentem kierunku Informatyka na Politechnice Świętokrzyskiej w Kielcach. Wcześniej ukończyłem Technikum w Zespole Szkół Elektrycznych w Kielcach, uzyskując tytuł Technika. Informatyką, a szczególnie tworzeniem stron internetowych i programowaniem zainteresowałem się już w gimnazjum. Od tego czasu, aż do teraz, tylko gdy mam wolny czas, staram się doskonalić swoje umiejętności.   
                  </p>
              </h4>
          </div>
      </div>
      
      <div class="row row_oferta">
          <div class="col-lg-12 text-center">
              <h1 id="offer">Oferta</h1>
          </div>
         <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 center-block text-center">
            <div class="thumbnail">
                <img src="src/pic/051-responsive.png" alt="Responsive">
                <div class="caption">
                    <h3>Profesjonalne strony www</h3>
                    <p>Dzięki responsywności strony internetowe są dostosowane do poprawnego wyświetlania na urządzeniach mobilnych, co jest przyjemne dla oka, a Ty zyskujesz przewagę nad konkurencją.</p>
                </div>
            </div> 
         </div>
         <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 center-block text-center">
            <div class="thumbnail">
                <img src="src/pic/051-analytics-1.png" alt="Analytics">
                <div class="caption">
                    <h3>Przemyślana strategia działania</h3>
                    <p>Nie liczy się tylko samo zrobienie strony internetowej, ale poźniejsza pomoc i wsparcie techniczne</p>
                </div>
            </div> 
         </div>
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 center-block text-center">
            <div class="thumbnail">
                <img src="src/pic/051-world-map-1.png" alt="Social Media">
                <div class="caption">
                   <h3>Portale społecznościowe</h3>
                    <p>Witryna internetowa powinna być też połączona z portalami społecznościowymi. Także w ten sposób zdobywaj klientów. Twoja strona obiegnie cały świat.</p> 
                </div>
            </div> 
         </div>
      </div>
      <div
      <div class="row row_portfolio">
        <div class="col-lg-12 text-center">
              <h1 id="portfolio">Moje prace</h1>
              
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 center-block text-center  ">
                  <a href="registration/index.php" target="_blank" class="portfolio-box">
                        <img src="src/pic/p1.png" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Szablon rejestracja w PHP
                                </div>
                                <div class="project-name">
                                    Prosty szablon rejestracji w PHP z dodatkiem zegarka w JavaScript.
                                </div>
                            </div>
                        </div>
                    </a>
               </div>
               <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 center-block text-center  ">
                  <a href="p_rwd/index.html" target="_blank" class="portfolio-box">
                        <img src="src/pic/p2.png" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Responsywna wizytówka
                                </div>
                                <div class="project-name">
                                    Wizytówka dostosowująca się do rodzielczości urządzeń mobilnych.
                                </div>
                            </div>
                        </div>
                    </a>
         </div>
           </div>
        </div>

        <div class="row row_skills">
         <div class="col-lg-12 text-center">
            <h1 id="skills">Skills</h1>
         </div>
            
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block col_skills text-center">
             
         </div>
        
        
        </div>
     
       <div class="row row_form">
        <div class="col-lg-8 col-lg-push-2 col_form text-center">
            <h2 id="contact">Skontaktuj się ze mną !</h2>
        </div>
        <div class="col-lg-8 col-lg-push-2 col_form">
         <form class="form" method="post" action="kontakt.php">
          <div class="form-group">
            
             <label for="nameField">Imię</label>
             <input type="text" class="form-control" name ="name" id="nameField" placeholder="Twoje imię">
          </div>
          
      
          <div class="form-group">
             <label for="emailField">E-mail</label>
             <input type="email" class="form-control" name ="email" id="emailField" placeholder="Twój adres e-mail">
          </div>
      
     
          <div class="form-group">
             <label for="descField">Treść</label>
             <textarea class="form-control" name ="message" id="descField" placeholder="Treść wiadomości"></textarea>
          </div>
          
          <button type="submit" class="btn btn-primary">Wyślij</button> <button type="reset" class="btn btn-default">Wyczyść</button>
          
         </form>
        </div>
       </div>
       
      <div class="row row_social">
            <div class="text-center center-block">
                <a href="https://www.facebook.com/profile.php?id=100002410543532" target="_blank"><i id="social-fb" class="fa fa-facebook-square fa-4x social"></i></a>
	            <a href="https://bitbucket.org/ZiqZaq/" target="_blank"><i id="social-bb" class="fa fa-bitbucket-square fa-4x social"></i></a>
	            <a href="mailto:michalborzecki95@wp.pl" target="_blank"><i id="social-em" class="fa fa-envelope-square fa-4x social"></i></a>
</div>
       </div>
  </div>
   </div>

    
    <script src="js/jquery-3.1.1.js"></script>
    <script src="js/bootstrap.js"></script>
        <!-- Scrolling Nav JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrolling-nav.js"></script>
    <!-- Skills -->
     <script src="js/skills.js"></script>  
</body>
</html>
